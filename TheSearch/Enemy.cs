﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Timers;

namespace TheSearch
{
    public abstract class Enemy : Actor
    {

        private int damage;
        private Random rand;
        private PointF playerPos;

        protected int Damage
        {
            get { return damage; }
        }
        
        protected PointF PlayerPos
        {
            get { return playerPos; }
        }

        protected Random Rand
        {
            get { return rand; }
        }

        protected Timer Timer { get; set; }

        protected Enemy(RectangleF bounds, Bitmap image, PointF playerPos, int speed, int health, int damage)
            : base (bounds, image, speed, health)
        {
            rand = new Random();
            this.damage = damage;
            this.playerPos = playerPos;
            Timer = new Timer(100);
            Timer.Elapsed += new ElapsedEventHandler(Act);
            Timer.Start();
        }

        public void UpdatePlayerPos(PointF playerPos) 
        {
            this.playerPos = playerPos;
        }

        protected abstract void Act(object sender, ElapsedEventArgs args);

        protected override void Collide(CollisionObject collider)
        {
            if (collider.GetType().Equals(typeof(Player)))
            {
                ((Player)collider).Hit(damage);
                
            }
        }
    }
}
