﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheSearch
{
    public abstract class Projectile : MovableObject
    {
        private bool friendly;

        protected int Damage { get; set; }
        protected bool Friendly { get { return friendly; } }

        protected Projectile(RectangleF bounds, Bitmap image, int damage, int speed, bool friendly)
            : base(bounds, image, speed)
        {
            Damage = damage;
            this.friendly = friendly;
        }
    }
}
