﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Timers;

namespace TheSearch
{
    public class Skeleton : Enemy
    {
        private Bow bow;
        private Timer shootTimer;

        public Skeleton(RectangleF bounds, Bitmap image, PointF playerPos)
            : base(bounds, image, playerPos, 2, 60, 5)
        {
            bow = new Bow(new RectangleF(-100, -100, 0, 0), null, new Bitmap("img/bowAnim.png"), new Bitmap("img/bowAnimHeavy.png"), new Bitmap("img/boltDark.png"), null, 20, -1, false);

            Moved += (p) => bow.ActorCenter = CalcCenter(); 

            shootTimer = new Timer(3000);
            shootTimer.Elapsed += new ElapsedEventHandler(Shoot);
            shootTimer.Start();
        }

        protected override void Act(object sender, ElapsedEventArgs args)
        {
            try {
                float x = Bounds.X - PlayerPos.X, y = Bounds.Y - PlayerPos.Y;

                RemoveDir(MoveDir.Up | MoveDir.Down | MoveDir.Right | MoveDir.Left);

                if (Math.Abs(x) < Math.Abs(y))
                {
                    if (x > 10)
                        Move(MoveDir.Left);
                    else if (x < -10)
                        Move(MoveDir.Right);
                }
                else
                {
                    if (y > 10)
                        Move(MoveDir.Up);
                    else if (y < -10)
                        Move(MoveDir.Down);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("SkeleAct");
                Console.WriteLine(e.StackTrace);
            }

        }

        private void Shoot(object sender, ElapsedEventArgs args)
        {
            try {
                int rotation;
                float x = Bounds.X - PlayerPos.X, y = Bounds.Y - PlayerPos.Y;

                if (Math.Abs(x) < Math.Abs(y))
                {
                    if (y > 0)
                        rotation = 0;
                    else
                        rotation = 180;
                }
                else
                {
                    if (x > 0)
                        rotation = 270;
                    else
                        rotation = 90;
                }

                bow.FastAttack(rotation, Bounds, null);

                shootTimer.Interval = Rand.Next(2500, 5000);
            }
            catch (Exception e)
            {
                Console.WriteLine("SkeleShoot");
                Console.WriteLine(e.StackTrace);
            }
        }

        public override void Draw(Graphics grph)
        {
            float tmp = Rotation;
            Rotation = 0;
            base.Draw(grph);
            Rotation = tmp;

            bow.Draw(grph);
        }
    }
}
