﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheSearch
{
    class Room : GameObject
    {
        private Wall[] walls;
        private Enemy[] enemies;

        public Enemy[] Enemies
        {
            get { return enemies; }
        }

        public Room(RectangleF bounds, Bitmap image, Enemy[] enemies, Wall[] walls)
            : base(bounds, image)
        {
            this.enemies = enemies;
            this.walls = walls;

            Wall northWall = new Wall(new Rectangle(0, 0, 1200, 110));
            Wall westWall = new Wall(new Rectangle(0, 0, 150, 600));
            Wall southWall = new Wall(new Rectangle(0, 490, 1200, 110));
            Wall eastWall = new Wall(new Rectangle(1050, 0, 150, 600));
            
            foreach (Enemy enemy in this.enemies)
                enemy.Died += new DeathHandler(OnActorDeath);
        }

        private void OnActorDeath(Actor actor)
        {
            enemies = (from enemy in enemies where enemy != actor select enemy).ToArray();
            CollisionObject.RemoveCollider(actor);
        }

        public override void Draw(Graphics grph)
        {
            base.Draw(grph);

            foreach (Wall wall in walls)
                wall.Draw(grph);

            foreach (Enemy enemy in enemies)
                enemy.Draw(grph);
        }
    }
}
