﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Timers;

namespace TheSearch
{
    class Ghoul : Enemy
    {
        private bool clipping;
        private Timer dashTimer;

        public Ghoul (RectangleF bounds, Bitmap image, PointF playerPos)
            : base(bounds, image, playerPos, 2, 100, 10)
        {
            Timer.Interval = 10;
            dashTimer = new Timer(5000);
            dashTimer.Elapsed += new ElapsedEventHandler(TryDash);
            dashTimer.Start();
        }

        protected override void Act(object sender, ElapsedEventArgs args)
        {
            try {
                PointF pos = Bounds.Location;
                if (!clipping)
                    Timer.Interval = 10;
                else
                    Timer.Interval = 750;

                if (!clipping)
                {
                    if (Math.Abs(pos.X - PlayerPos.X) <= 30)
                        RemoveDir(MoveDir.Right | MoveDir.Left);
                    else if (pos.X - PlayerPos.X > 0)
                        Move(MoveDir.Left);
                    else
                        Move(MoveDir.Right);

                    if (Math.Abs(pos.Y - PlayerPos.Y) <= 30)
                        RemoveDir(MoveDir.Up | MoveDir.Down);
                    else if (pos.Y - PlayerPos.Y > 0)
                        Move(MoveDir.Up);
                    else
                        Move(MoveDir.Down);
                }
                else
                {
                    RemoveDir(MoveDir.Right | MoveDir.Left);
                    RemoveDir(MoveDir.Up | MoveDir.Down);

                    MoveDir[] player = new MoveDir[2];

                    if (pos.X - PlayerPos.X > 0)
                        player[0] = MoveDir.Left;
                    else
                        player[0] = MoveDir.Right;

                    if (pos.Y - PlayerPos.Y > 0)
                        player[1] = MoveDir.Up;
                    else
                        player[1] = MoveDir.Down;

                    MoveDir move = player[Rand.Next(2)];
                    Move(move);
                    clipping = false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("GhoulAct");
                Console.WriteLine(e.StackTrace);
            }
        }

        private void TryDash(object sender, ElapsedEventArgs args)
        {
            try {
                if (Rand.Next(100) < 60 && Speed < 5)
                    Speed += 1;
                else if (Speed > 1)
                    Speed -= 1;
            }
            catch (Exception e)
            {
                Console.WriteLine("TryDash");
                Console.WriteLine(e.StackTrace);
            }
        }

        protected override void Collide(CollisionObject collider)
        {
            base.Collide(collider);

            if (collider.GetType().Equals(typeof(Wall)))
                clipping = true;
        }

        public override void Draw(Graphics grph)
        {
            float tmp = Rotation;
            Rotation = 0;
            base.Draw(grph);
            Rotation = tmp;
        }
    }
}
