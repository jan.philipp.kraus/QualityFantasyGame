﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheSearch
{
    public class Bow : Weapon
    {
        private Bitmap boltImg;
        private Bitmap boltHeavyImg;
        private List<Bolt> bolts;
        private int boltCount, defaultBoltCount;
        private bool friendly;

        public Bow (RectangleF bounds, Bitmap image, Bitmap fastAnimation, Bitmap heavyAnimation, Bitmap boltFast, Bitmap boltHeavy, int damage, int boltCount, bool friendly)
            : base(bounds, image, fastAnimation, heavyAnimation, damage, 200)
        {
            this.boltCount = boltCount;
            defaultBoltCount = boltCount;
            this.friendly = friendly;
            bolts = new List<Bolt>();
            boltImg = boltFast;
            boltHeavyImg = boltHeavy;
        }

        public void AddBolt(Bolt bolt)
        {
            bolts.Remove(bolt);
            boltCount++;
        }

        public void Reset()
        {
            boltCount = defaultBoltCount;
            bolts = new List<Bolt>();
        }

        public override void FastAttack(int playerRotation, RectangleF playerBounds, Enemy[] enemies)
        {
            PointF actorCenter = new PointF(playerBounds.X + playerBounds.Width / 2.0f, playerBounds.Y + playerBounds.Height / 2.0f);
            RectangleF animationBounds = new RectangleF(new PointF(-FastAnimation.Width / 2.0f, -FastAnimation.Height), FastAnimation.Size);
            StartAnimation(playerRotation, actorCenter, FastAnimation, animationBounds);
            if (boltCount > 0 || boltCount <= -1)
            {
                
                PointF middle = new PointF(actorCenter.X - boltImg.Width / 2.0f, actorCenter.Y - boltImg.Height / 2.0f);
                bolts.Add(new Bolt(new RectangleF(middle, boltImg.Size), boltImg, Damage, friendly));


                if (playerRotation < 90 || playerRotation > 270)
                    bolts.Last().Move(MoveDir.Up);
                else if (playerRotation > 90 && playerRotation < 270)
                    bolts.Last().Move(MoveDir.Down);

                if (playerRotation > 0 && playerRotation < 180)
                    bolts.Last().Move(MoveDir.Right);
                else if (playerRotation < 360 && playerRotation > 180)
                    bolts.Last().Move(MoveDir.Left);

                boltCount--;
            }
        }

        public override void HeavyAttack(int playerRotation, RectangleF playerBounds, Enemy[] enemies)
        {
            PointF actorCenter = new PointF(playerBounds.X + playerBounds.Width / 2.0f, playerBounds.Y + playerBounds.Height / 2.0f);
            RectangleF animationBounds = new RectangleF(new PointF(-HeavyAnimation.Width / 2.0f, -HeavyAnimation.Height), HeavyAnimation.Size);
            StartAnimation(playerRotation, actorCenter, HeavyAnimation, animationBounds);
            if (boltCount > 0 || boltCount <= -1)
            {
                PointF middle = new PointF(actorCenter.X - boltHeavyImg.Width / 2.0f, actorCenter.Y - boltHeavyImg.Height / 2.0f);
                bolts.Add(new Bolt(new RectangleF(middle, boltHeavyImg.Size), boltHeavyImg, Damage * 2, friendly));


                if (playerRotation < 90 || playerRotation > 270)
                    bolts.Last().Move(MoveDir.Up);
                else if (playerRotation > 90 && playerRotation < 270)
                    bolts.Last().Move(MoveDir.Down);

                if (playerRotation > 0 && playerRotation < 180)
                    bolts.Last().Move(MoveDir.Right);
                else if (playerRotation < 360 && playerRotation > 180)
                    bolts.Last().Move(MoveDir.Left);

                boltCount--;
            }
        }

        public override void Draw(Graphics grph)
        {
            base.Draw(grph);
            grph.DrawString(boltCount.ToString(), new Font(FontFamily.GenericSansSerif, 20), new SolidBrush(Color.Black), Bounds);
            for (int i = 0; i < bolts.Count; i++)
            {
                if (bolts[i] != null)
                    bolts[i].Draw(grph);
            }
        }
    }
}
