﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheSearch
{
    public class Melee : Weapon
    {
        private int attackRange;

        public Melee(RectangleF bounds, Bitmap image, Bitmap fastAnimation, Bitmap heavyAnimation, int damage, int attackRange)
            : base(bounds, image, fastAnimation, heavyAnimation, damage)
        {
            this.attackRange = attackRange;
        }

        public override void FastAttack(int playerRotation, RectangleF playerBounds, Enemy[] enemies)
        {
 	        RectangleF attackArea;
            switch (playerRotation)
            {
                case 0:
                   attackArea = new RectangleF(new PointF(playerBounds.X, playerBounds.Y - attackRange), new SizeF(playerBounds.Width, attackRange + playerBounds.Height));
                    break;
                case 90:
                    attackArea = new RectangleF(new PointF(playerBounds.X + playerBounds.Width, playerBounds.Y), new SizeF(attackRange, playerBounds.Height));
                    break;
                case 180:
                    attackArea = new RectangleF(new PointF(playerBounds.X, playerBounds.Y + playerBounds.Height), new SizeF(playerBounds.Width, attackRange));
                    break;
                case 270:
                    attackArea = new RectangleF(new PointF(playerBounds.X - attackRange, playerBounds.Y), new SizeF(attackRange, playerBounds.Height));
                    break;
                default:
                    attackArea = playerBounds;
                    break;
            }
            RectangleF animationBounds = new RectangleF(new PointF(-FastAnimation.Width / 2.0f, -FastAnimation.Height), FastAnimation.Size);
            StartAnimation(playerRotation, new PointF(playerBounds.X + playerBounds.Width / 2.0f, playerBounds.Y + playerBounds.Height / 2.0f), FastAnimation, animationBounds);

            Enemy[] victims = (from enemy in enemies where enemy.Bounds.IntersectsWith(attackArea) select enemy).ToArray();
            foreach (Enemy victim in victims)
                victim.Hit(Damage);
        }

        public override void HeavyAttack(int playerRotation, RectangleF playerBounds, Enemy[] enemies)
        {
            PointF center = new PointF(playerBounds.X + playerBounds.Width / 2.0f, playerBounds.Y + playerBounds.Height / 2.0f);
            RectangleF attackArea = new RectangleF(new PointF(center.X - HeavyAnimation.Width / 2.0f, center.Y - HeavyAnimation.Height / 2.0f), HeavyAnimation.Size);
            RectangleF animationBounds = new RectangleF(new PointF(-HeavyAnimation.Width / 2.0f, -HeavyAnimation.Height / 2.0f), HeavyAnimation.Size);
            StartAnimation(playerRotation, center, HeavyAnimation, animationBounds);

            Enemy[] victims = (from enemy in enemies where enemy.Bounds.IntersectsWith(attackArea) select enemy).ToArray();
            foreach (Enemy victim in victims)
                victim.Hit(Damage * 2);
        }
    }
}
