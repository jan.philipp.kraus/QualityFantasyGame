﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Timers;

namespace TheSearch
{
    class Bat : Enemy
    {

        public Bat(RectangleF bounds, Bitmap image)
            : base(bounds, image, new PointF(0,0), 2, 50, 10) 
        {
            Timer.Interval = 1;
        }

        protected override void Act(object sender, ElapsedEventArgs args)
        {
            try {
                MoveDir dir = (MoveDir)((int)Math.Pow(2, Rand.Next(4)));

                if (Rand.Next(2) == 0)
                    RemoveDir(dir);
                else
                    Move(dir);

                Timer.Interval = Rand.Next(100, 750);
            }
            catch (Exception e)
            {
                Console.WriteLine("BatAct");
                Console.WriteLine(e.StackTrace);
            }
        }
    }
}
